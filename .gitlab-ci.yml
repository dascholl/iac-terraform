## -------------------------------------------------------------------------------
### Required Project or Group LEVEL Variables 
###    ARM_TENANT_ID
###    ARM_SUBSCRIPTION_ID
###    ARM_CLIENT_ID
###    ARM_CLIENT_SECRET
###    ARM_ACCESS_KEY
###    TF_VAR_remote_state_account
###    TF_VAR_remote_state_container

variables:
  TF_VERSION: 0.12.24
  GO_VERSION: 1.12.14
  TEMPLATE: samples/webdata
  TEST_CASE: testWebData
  TF_WORKSPACE_NAME: gitlab-wds

stages:
  - prepare
  - build
  - deploy
  - validate

image:
  name: hashicorp/terraform:$TF_VERSION
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

.debug: &debug
  before_script:
    - env |grep CI

.install_tf_alpine: &install_tf_alpine
  before_script:
    - currdir=$(pwd)
    - cd /tmp
    - wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
    - unzip terraform_${TF_VERSION}_linux_amd64.zip -d /usr/bin
    - cd $currdir
    - /usr/bin/terraform version

.terraform_flow: &terraform_flow
  before_script:
    - cd $TEMPLATE
    - rm -rf .terraform
    - terraform init -backend-config "storage_account_name=${TF_VAR_remote_state_account}" -backend-config "container_name=${TF_VAR_remote_state_container}"
    - terraform workspace new $TF_WORKSPACE_NAME || terraform workspace select $TF_WORKSPACE_NAME
    - terraform workspace list

tf_lint:
  stage: prepare
  script:
    - terraform fmt -recursive -check

tf_validate:
  <<: *terraform_flow
  stage: prepare
  script:
    - terraform validate

go_lint:
  stage: prepare
  image:
    name: "golang:$GO_VERSION"
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/go/bin:/usr/local/go/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  script:
    - FILES_WITH_FMT_ISSUES=$(find . -name "*.go" | grep -v '.terraform' | xargs gofmt -l | wc -l)
    - FILES_WITH_FMT_ISSUES=$(($FILES_WITH_FMT_ISSUES + 0))
    - (if [[ $FILES_WITH_FMT_ISSUES == 0 ]]; then echo "✓ Go Lint Success!"; else find . -name "*.go" | grep -v '.terraform' | xargs gofmt -l; exit -1; fi);

unit_test:
  <<: *install_tf_alpine
  stage: prepare
  image:
    name: "golang:${GO_VERSION}-alpine"
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/go/bin:/usr/local/go/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  script:
    - (if [[ "$(go version)" == "go version go${GO_VERSION} linux/amd64" ]]; then echo "✓ Go binary installed!"; else echo "Go binary not installed!"; /bin/false; fi);
    - (if [[ "$(terraform version)" == "Terraform v${TF_VERSION}" ]]; then echo "✓ Terraform installed!"; else echo "Terraform not installed"; /bin/false; fi);
    - currdir=$(pwd)
    - apk add --no-cache gcc libc-dev bind-tools git
    - go get -u -d github.com/magefile/mage
    - magedir=$(find /go/pkg/mod/ -name magefile -type d | grep -v cache)
    - cd $magedir/$(ls $magedir/)
    - go run bootstrap.go
    - cd $currdir
    - mage test

tf_plan:
  <<: *terraform_flow
  variables:
    TF_PLAN_FILE: "${TF_WORKSPACE_NAME}_plan.out"
  stage: build
  script:
    - terraform plan -input=false -out $TF_PLAN_FILE
  dependencies:
    - tf_validate
  artifacts:
    paths:
      - $TEMPLATE/$TF_PLAN_FILE

tf_apply:
  <<: *terraform_flow
  variables:
    TF_PLAN_FILE: "${TF_WORKSPACE_NAME}_plan.out"
  stage: deploy
  script:
    - terraform apply -input=false $TF_PLAN_FILE
  dependencies:
    - tf_plan

int_test:
  <<: *install_tf_alpine
  stage: validate
  image:
    name: "golang:${GO_VERSION}-alpine"
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/go/bin:/usr/local/go/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  script:
    - (if [[ "$(go version)" == "go version go${GO_VERSION} linux/amd64" ]]; then echo "✓ Go binary installed!"; else echo "Go binary not installed!"; /bin/false; fi);
    - (if [[ "$(terraform version)" == "Terraform v${TF_VERSION}" ]]; then echo "✓ Terraform installed!"; else echo "Terraform not installed"; /bin/false; fi);
    - currdir=$(pwd)
    - apk add --no-cache gcc libc-dev bind-tools git
    - go get -u -d github.com/magefile/mage
    - magedir=$(find /go/pkg/mod/ -name magefile -type d | grep -v cache)
    - cd $magedir/$(ls $magedir/)
    - go run bootstrap.go
    - cd $currdir
    - cd $TEMPLATE
    - rm -rf .terraform
    - terraform init -backend-config "storage_account_name=${TF_VAR_remote_state_account}" -backend-config "container_name=${TF_VAR_remote_state_container}"
    - terraform workspace new $TF_WORKSPACE_NAME || terraform workspace select $TF_WORKSPACE_NAME
    - terraform workspace list
    - cd $currdir
    - mage $TEST_CASE